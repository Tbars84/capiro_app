﻿document.addEventListener('deviceready', function onDeviceReady() {
    //angular.bootstrap(document, ['starter']);
}, false);

var appAng = angular.module('starter', ['ngCordova', 'ngRoute']);
var db = null;
var cedula = null;
var finca = null;
var persona = null;

function onFail(message) {
    alert('Failed because: ' + message);
}

function titleCase(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function alertCallback() {

}

function iniciarSistema($scope, $location, $cordovaSQLite, $window) {
	try {
		//db = window.sqlitePlugin.openDatabase({name: 'dgsign.db', location: 'default'});//solo funca cuando está creada
	    db = window.openDatabase("capiro.db'", '1.0', 'capiro.db', 1024 * 1024 * 100);
        var queryCrear = "CREATE TABLE IF NOT EXISTS votos(cedula text, persona integer, finca integer, CONSTRAINT votos_pk PRIMARY KEY (cedula))";
	    db.transaction(function (tx) {
	        tx.executeSql(queryCrear, [],
                function(tx, res) {

                },
			    function (errorIn) {
				    alert("Error interno votos: " + errorIn);
			    }, function (success) {
			        alert('success votos ' + success);
			    }
		    );
	    }, function (error) {
	        alert("Error externo: " + error);
	    }, function (success) {
	        alert('successssss');
	    });
	} catch(errExt) {
		alert("Error en la aplicación: " + errExt.message);
	}
}

appAng.config(function ($routeProvider) {
    /*$routeProvider.when('/',
	{
	    controller: 'FincasCtrl',
	    templateUrl: 'templates/Finca.html'
	})
	.when('/finca',
	{
	    controller: 'MainCtrl',
	    templateUrl: 'templates/Inicio.html'
	})*/
    
    $routeProvider.when('/',
	{
	    controller: 'MainCtrl',
		templateUrl: 'templates/Inicio.html'
	})
	.when('/finca',
	{
	    controller: 'FincasCtrl',
		templateUrl: 'templates/Finca.html'
	})
    .when('/personas',
	{
	    controller: 'PersonasCtrl',
	    templateUrl: 'templates/Personas.html'
	})
    .when('/enviar',
	{
	    controller: 'EnviarCtrl',
	    templateUrl: 'templates/Enviar.html'
	})
});

appAng.controller('MainCtrl', function ($scope, $location, $cordovaSQLite, $window) {
	try {
		if($window.cordova) {
			iniciarSistema($scope, $location, $cordovaSQLite, $window);
			alert('main33');
		}
	} catch(error) {
		alert('error');
		alert(error);
	}

	$scope.colocarCedula = function () {
	    cedula = frmCedula.txtCedula.value;
	    var siNumero = isNumeric(cedula);
	    if (siNumero == true) {
	        alert('ssdsd');
	        //$window.location('/finca');
	        $location.path('/finca');
	    } else {
	        navigator.notification.alert("El campo debe ser numérico", alertCallback, "Campo no válido", "Aceptar")
	    }  
	};

});

appAng.controller('FincasCtrl', function ($scope, $location) {
    alert('inicial : ');
    ini( $(window).width() );
    $scope.colocarFinca = function (fincaLocal) {
        finca = fincaLocal;
        alert('finca : ' + finca);
        $location.path('/personas');

    };
});

appAng.controller('PersonasCtrl', function ($scope, $location) {
    alert('personas : ');
    ini( $(window).width() );
    $scope.personasBiArr = [{
        '1': [{
            'valor': '1',
            'nombre': 'one1',
            'imagen': 'img1.png'
        }, {
            'valor': '2',
            'nombre': 'two1',
            'imagen': 'img2.png'
        }, {
            'valor': '3',
            'nombre': 'three1',
            'imagen': 'img3.png'
        }]
    }, {
        '2': [{
            'valor': '4',
            'nombre': 'one2',
            'imagen': 'img4.png'
        }, {
            'valor': '5',
            'nombre': 'two2',
            'imagen': 'img5.png'
        }, {
            'valor': '6',
            'nombre': 'three2',
            'imagen': 'img6.png'
        }]
    }];

    $scope.personasArr = $scope.personasBiArr[parseInt(finca-1)];
    
    $scope.colocarPersona = function (personaLocal) {
        persona = personaLocal;
        alert('persona : ' + persona);
        alert('finca : ' + finca);
        alert('cedula : ' + cedula);
        
        db = window.openDatabase("capiro.db'", '1.0', 'capiro.db', 1024 * 1024 * 100);
        //var queryInsertar = "INSERT INTO votos(cedula, persona, finca) VALUES('" + cedula + "', " + persona + ", " + finca + ")";
        var queryInsertar = "INSERT INTO votos(cedula, persona) VALUES('" + cedula + "', " + persona +")";
        db.transaction(function (tx) {
            tx.executeSql(queryInsertar, [],
                function (tx, res) {

                    var querySel = "SELECT cedula, persona FROM votos";
                    tx.executeSql(querySel, [], function (tx, res) {
                        var cantidadRegistros = res.rows.length;
                        alert('canti : ' + cantidadRegistros);
                        if (cantidadRegistros > 0) {
                            var j = 0;
                            while (j < cantidadRegistros) {
                                var object = res.rows.item(j);
                                var cedulainLocal = object.cedula;
                                var personainLocal = object.persona;
                                alert('cedula : ' + cedulainLocal);
                                alert('persona1 : ' + personainLocal);
                                j++;
                            }
                            $scope.$applyAsync();
                        }
                    }, function (error) {
                        alert("Error: " + error);
                    }, function (success) {
                        alert('success internos');
                    });

                },
			    function (errorIn) {
			        alert("Error interno votos: " + errorIn);
			    }, function (success) {
			        alert('success votos ' + success);
			    }
		    );
        }, function (error) {
            alert("Error externo base: " + error);
        }, function (success) {
            alert('successssss abrir base');
        });

    };
});

appAng.controller('EnviarCtrl', function ($scope, $location, $cordovaSQLite, $http) {
    alert('enviar : ');

    function successEnvio() {
        alert('success enviar : ');
    }

    function errorEnvio() {
        alert('error enviar : ');
    }

    function onConfirm(buttonIndex) {
        alert('boton : ' + buttonIndex);
        if(parseInt(buttonIndex) == 1) {
            db = window.openDatabase("capiro.db'", '1.0', 'capiro.db', 1024 * 1024 * 100);
            var querySel = "SELECT cedula, persona FROM votos";
            db.transaction(function (tx) {
                tx.executeSql(querySel, [],
                    function (tx, res) {
                    
                        var cantidadRegistros = res.rows.length;
                        alert('canti : ' + cantidadRegistros);
                        if (cantidadRegistros > 0) {
                            var j = 0;
                            while (j < cantidadRegistros) {
                                var object = res.rows.item(j);
                                var cedulainLocal = object.cedula;
                                var personainLocal = object.persona;
                                alert('cedula : ' + cedulainLocal);
                                alert('persona1 : ' + personainLocal);
                                jsonObj = [];
                                item = {}
                                item["cedula"] = cedulainLocal;
                                item["persona"] = personainLocal;
                                jsonObj.push(item);
                                $http({
                                    method: "POST",
                                    url: "highlander.gvbagencia.co/Capiro/",
                                    data: angular.toJson(jsonObj),
                                    headers: {
                                        'Content-Type': 'application/json'
                                    }
                                }).then(successEnvio, errorEnvio);

                                j++;
                            }
                            $scope.$applyAsync();
                            

                        }
                    },
                    function (errorIn) {
                        alert("Error interno votos: " + errorIn);
                    }, function (success) {
                        alert('success votos ' + success);
                    }
                );
            }, function (error) {
                alert("Error externo base: " + error);
            }, function (success) {
                alert('successssss abrir base');
            });
        }
    }

    $scope.enviarDatos = function (fincaLocal) {
        navigator.notification.confirm(
            'Esta seguro de enviar los datos', 
             onConfirm, 
            'Enviar Datos',    
            ['Enviar', 'Cancelar'] 
        );
    };
    //db = window.openDatabase("capiro.db'", '1.0', 'capiro.db', 1024 * 1024 * 100);
});
$(document).ready(function(){
ini( $(window).width() );
    $( window ).resize(function() {
        ini( $(window).width() );
    });
})
function ini(Wh){
    if (Wh > 992) {
        $('.main').css({'height': (Wh * 0.625) , 'max-height' : '917px'});
    }else{
        $('.main').css({'height': (Wh * 0.769) , 'background' : 'url(images/bg-rosas-4-3.jpg) center center no-repeat' , 'background-size' : 'cover'});

    }
}